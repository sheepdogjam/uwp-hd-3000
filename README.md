# Microsoft LifeCam HD-3000 を UWPから操作するサンプルコード

参考 一日一事未満 : http://sheepdogjam.cocolog-nifty.com/blog/2018/10/microsoft-lifec.html

Microsoft LifeCam HD-3000をWindows10 UWPで操作するサンプルコードです。
Windows10で利用可能なカメラであれば、LifeCam HD-3000以外のカメラも
利用可能だと思います。

※本ソースコードにより利用者及び第三者に損害を与えないことを保証しません。また一切の保証は致しません。

