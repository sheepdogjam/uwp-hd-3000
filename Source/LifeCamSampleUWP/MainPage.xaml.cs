﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Media;
using Windows.Media.Capture;
using Windows.Media.Core;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// 空白ページの項目テンプレートについては、https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x411 を参照してください

namespace LifeCamSampleUWP
{
    /// <summary>
    /// それ自体で使用できる空白ページまたはフレーム内に移動できる空白ページ。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private const int _capacity = 2048;
        private const int _maxImages = 3;
        private Status _status = new Status();

        public MainPage()
        {
            this.InitializeComponent();
            GetStatus();
        }

        private void GetStatus()
        {
            this.textFolderName.Text = _status.FolderName;
            this.textBaseFileName.Text = _status.BaseFileName;
            this.textConditionName.Text = _status.ConditionName;
            this.textOutputFileName.Text = _status.OutputFileNameNotIncrement;
            this.textCounter.Text = _status.Counter.ToString();
        }

        private void SetStatus()
        {
            _status = new Status(this.textFolderName.Text, this.textBaseFileName.Text, this.textConditionName.Text);
            GetStatus();
        }

        private void textFolderName_TextChanged(object sender, TextChangedEventArgs e) => SetStatus();

        private async Task<DeviceInformation> GetDevice()
        {
            var devices = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);
            var firstDevice = devices[0];
            foreach (var device in devices)
            {
                // プロファイルを持っているデバイスがある場合はそちらを返す
                if (MediaCapture.IsVideoProfileSupported(device.Id) && device.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Back)
                {
                    firstDevice = device;
                    break;
                }
            }
            return firstDevice;
        }

        private void ShowCameraInformation(DeviceInformation device)
        {
            var informationString = new StringBuilder(_capacity);
            informationString.Append("Name\t: ").Append(device.Name).Append("\r\n");
            informationString.Append("Kind\t: ").Append(device.Kind).Append("\r\n");
            this.textBlockCameraInformations.Text = informationString.ToString();
        }

        private List<MediaCaptureVideoProfile> GetProfile(DeviceInformation device)
        {
            if (device.Id == "")
            {
                ShowRequestMessageAsync("プロファイルをサポートしたカメラデバイスが見つかりません。");
                return null;
            }
            return MediaCapture.FindAllVideoProfiles(device.Id).ToList();
        }

        private void ShowCameraProfile(List<MediaCaptureVideoProfile> profileList)
        {
            var profileString = new StringBuilder(_capacity);
            foreach(var profile in profileList)
                foreach (var _description in profile.SupportedRecordMediaDescription)
                {
                    profileString.Append("Profile ----------------------------").Append("\r\n");
                    profileString.Append("Width:").Append(_description.Width).Append("\r\n");
                    profileString.Append("Height:").Append(_description.Height).Append("\r\n");
                    profileString.Append("Framerate:").Append(_description.FrameRate).Append("\r\n");
                    profileString.Append("------------------------------------").Append("\r\n");
                }
            this.textBlockCameraProfiles.Text = profileString.ToString();
        }

        private async Task StartCapture()
        {
            // Windows10に用意されているオブジェクトWindows10が認識しているカメラなら利用可能?
            var _capture = new MediaCapture();
            try
            {
                // Webカメラの初期化
                await _capture.InitializeAsync();
            }
            catch (UnauthorizedAccessException ex)
            {
                // Web カメラを許可していない場合はここで例外が発生する
                ShowRequestMessageAsync(ex);
                return;
            }

            // 一番大きなプレビューサイズのものを取得する
            var resolutions = GetPreviewResolusions(_capture);
            var maxResolution = resolutions[0];
            foreach (var vp in resolutions)
                if (vp.Width > maxResolution.Width && vp.Subtype.Equals("YUY2"))
                    maxResolution = vp;

            // 取得したプレビューサイズをMediaCaptureオブジェクトに設定する
            await _capture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.VideoPreview, maxResolution);

            // MediaCapture インスタンスを CaptureElement コントロールに設定する
            previewImage.Source = _capture;

            // プレビューを開始する
            await _capture.StartPreviewAsync();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // デバイス情報の表示
            var _device = (GetDevice()).Result;
            ShowCameraInformation(_device);
            ShowCameraProfile(GetProfile(_device));
            await StartCapture();
        }

        private async void Page_GettingFocus(UIElement sender, GettingFocusEventArgs args) => await StartCapture();

        private async void ButtonShootPicture(object sender, RoutedEventArgs e)
        {
            if (previewImage.Source == null) return;

            // プレビューに付けた MediaCapture オブジェクトを取り出す
            var capture = previewImage.Source;

            // 保存する画像ファイルのフォーマット (pngファイルを指定)
            var imageProperties = Windows.Media.MediaProperties.ImageEncodingProperties.CreatePng();

            // 撮影する
            await KnownFolders.PicturesLibrary.CreateFolderAsync(_status.FolderName, CreationCollisionOption.OpenIfExists);
            var file = await KnownFolders.PicturesLibrary.CreateFileAsync(_status.FolderName + @"\" + _status.OutputFileName, CreationCollisionOption.GenerateUniqueName);
            try
            {
                await capture.CapturePhotoToStorageFileAsync(imageProperties, file);
            }
            catch (Exception ex)
            {
                ShowRequestMessageAsync(ex);
                return;
            }
            using (var stream = await file.OpenReadAsync())
            {
                var bi = new BitmapImage();
                bi.SetSource(stream);
                var image = new Image() { Source = bi };
                if (imagePanel.Children.Count == _maxImages) imagePanel.Children.RemoveAt(0);
                imagePanel.Children.Add(image);
            }
            GetStatus();
        }

        /// <summary>
        /// エラーダイアログの表示
        /// <param name="ex"></param>
        private void ShowRequestMessageAsync(Exception ex) => ShowRequestMessageAsync(ex.ToString());
        private async void ShowRequestMessageAsync(string message) => await (new Windows.UI.Popups.MessageDialog($"エラー : {message}", "Web カメラが使えません")).ShowAsync();

        private List<VideoEncodingProperties> GetPreviewResolusions(MediaCapture capture)
        {
            var ret = capture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoPreview);
            // リソースが取得できなかった場合は空のリストを返す
            if (ret.Count <= 0)
                return new List<VideoEncodingProperties>();
            return ret.Select(item => (VideoEncodingProperties)item).ToList();
        }
    }
}
