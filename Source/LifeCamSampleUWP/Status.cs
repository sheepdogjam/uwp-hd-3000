﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifeCamSampleUWP
{
    class Status
    {
        private int _counter = 0;
        private const string _separater = "_";

        public Status()
        {

        }

        public Status(string folderName, string baseFileName, string conditionName)
        {
            FolderName = folderName ?? FolderName;
            BaseFileName = baseFileName ?? BaseFileName;
            ConditionName = conditionName ?? ConditionName;
        }

        public string FolderName { get; } = "LifeCam";
        public string BaseFileName { get; } = "pic";
        public string ConditionName { get; } = "01";
        public string OutputFileNameNotIncrement {
            get { return BaseFileName + _separater + ConditionName + _separater + _counter.ToString() + ".png"; } }

        public string OutputFileName
        {
            get
            {
                var temp = OutputFileNameNotIncrement;
                _counter++;
                return temp;
            }
        }
        public int Counter
        {
            get { return _counter; }
        }
    }
}
